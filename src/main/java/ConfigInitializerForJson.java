import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfigInitializerForJson implements ConfigInitializer {

    private static final Logger LOGGER = Logger.getLogger(ConfigInitializerForJson.class);

    public List<FileSuffix> initialConfig() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Arrays.asList(objectMapper.readValue(Paths.get("config.json").toFile(), FileSuffix[].class));
        } catch (IOException e) {
            LOGGER.error(e + "in " + getClass());
            return new ArrayList<FileSuffix>();
        }

    }
}
