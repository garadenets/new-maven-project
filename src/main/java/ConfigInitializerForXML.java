import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigInitializerForXML implements ConfigInitializer {
    private static final Logger LOGGER = Logger.getLogger(ConfigInitializerForXML.class);

    public List<FileSuffix> initialConfig() throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("config.xml"));
        NodeList fileSuffixElement = document.getDocumentElement().getElementsByTagName("file");
        LOGGER.info("File config from was read");
        List<FileSuffix> allFileSuffix = new ArrayList<>();
        for (int i = 0; i < fileSuffixElement.getLength(); i++) {
            Node fileSuffix = fileSuffixElement.item(i);
            NamedNodeMap attributes = fileSuffix.getAttributes();
            allFileSuffix.add(new FileSuffix(attributes.getNamedItem("name").getNodeValue(), attributes.getNamedItem("suffix").getNodeValue()));
        }
        return allFileSuffix;

    }
}
