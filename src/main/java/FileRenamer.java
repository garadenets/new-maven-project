import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

public class FileRenamer {

    private static final Logger LOGGER = Logger.getLogger(FileRenamer.class);

    public void renameFiles(List<FileSuffix> filesAndSuffix) {
        if ((filesAndSuffix != null) && (allFilesExist(filesAndSuffix))) {
            for (FileSuffix file : filesAndSuffix) {
                File fileToRename = new File(file.getName());
                String[] nameFile = file.getName().split("\\.");
                fileToRename.renameTo(new File(nameFile[0] + file.getSuffix() + "." + nameFile[1]));
                LOGGER.debug(file.getName() + " rename to " + nameFile[0] + file.getSuffix() + "." + nameFile[1]);
            }
            LOGGER.info("All files was renamed. Program finished");
        }

    }

    public boolean allFilesExist(List<FileSuffix> filesAndSuffix) {
        for (FileSuffix file : filesAndSuffix) {
            if (!new File(file.getName()).exists()) {
                LOGGER.error(getClass().getName() + ": File does't exist");
                return false;
            }
        }
        return true;
    }
}
