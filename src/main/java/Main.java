

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {

        LOGGER.info("Program start");
        long timeStart = new Date().getTime();
        ConfigInitializerForXML init = new ConfigInitializerForXML();
        FileRenamer fileRenamer = new FileRenamer();
        List<FileSuffix> filesAndSuffix = init.initialConfig();
        fileRenamer.renameFiles(filesAndSuffix);
        long timeFinish = new Date().getTime();
        LOGGER.info("Program end");

        ResultRecoderXML resultRecoderXML = new ResultRecoderXML();
        resultRecoderXML.writerXML(filesAndSuffix, timeFinish - timeStart);
    }
}
