import org.apache.log4j.Logger;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.util.List;

public class ResultRecoderXML {

    private static final Logger LOGGER = Logger.getLogger(ResultRecoderXML.class);

    public void writerXML(List<FileSuffix> fileSuffixes, long time) {

        try {
            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = outputFactory.createXMLStreamWriter(new FileWriter("result.xml"));
            writer.writeStartDocument("1.0");
            writer.writeStartElement("Files");
            writer.writeAttribute("NameConfigFile", "config.xml");
            writer.writeStartElement("ExecutionTime");
            writer.writeCharacters(String.valueOf(time) + " milliseconds");
            writer.writeEndElement();
            for (FileSuffix file : fileSuffixes) {
                String[] nameFile = file.getName().split("\\.");
                writer.writeStartElement("file");
                writer.writeStartElement("OldName");
                writer.writeCharacters(file.getName());
                writer.writeEndElement();
                writer.writeStartElement("NewName");
                writer.writeCharacters(nameFile[0] + file.getSuffix() + "." + nameFile[1]);
                writer.writeEndElement();
                writer.writeEndElement();
            }


            writer.writeEndElement();
            writer.close();
            LOGGER.info("Result was record in XML");
        } catch (Exception exception) {
            LOGGER.error(exception);
        }
    }
}
